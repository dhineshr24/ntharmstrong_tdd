import math;
import os
import sys;


class Armstrong:

    def isArmstrong(self, val: int) -> bool:

        parts = [int(_) for _ in str(val)]
        counter = 0
        for _ in parts:
            counter += _ ** 3
        return (counter == val)

    def NthArmstrong(self, n):

        count = 0;

        for i in range(1, sys.maxsize):

            num = i;
            rem = 0;
            digit = 0;
            sum = 0;

            # Copy the value for num in num
            num = i;

            # Find total digits in num
            digit = int(math.log10(num) + 1);

            # Calculate sum of power of digits
            while (num > 0):
                rem = num % 10;
                sum = sum + pow(rem, digit);
                num = num // 10;

            # Check for Armstrong number
            if (i == sum):
                count += 1;
            if (count == n):
                return i;

    def readFromFile(self, filename):
        if not os.path.exists(filename):
            raise Exception
        infile = open(filename, "r")
        line = infile.readlines()
        for x in line:
            print(f"The {int(x)}th Armstrong Number is {self.NthArmstrong(int(x))}")
        return line


ob = Armstrong()

ob.readFromFile("TestCases.txt")